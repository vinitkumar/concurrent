#! /usr/bin/env python

import threading
import time

init = threading.Event()

def worker():
    init.wait()
    print "I am worker"

def initialize():
    print "Initializing some data"
    time.sleep(10)
    print "Unblocking the workers"
    init.set()

# Launch a bunch of worker threads
threading.Thread(target=worker).start()
threading.Thread(target=worker).start()
threading.Thread(target=worker).start()
threading.Thread(target=worker).start()

initialize()