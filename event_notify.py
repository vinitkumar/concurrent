#! /usr/bin/env python

import threading
import time


item = None

available = threading.Semaphore(0)

completed = threading.Event()


# A worker thread
def worker():
    while True:
        available.acquire()
        print "Worker processing", item
        time.sleep(5)
        print "worker: done"
        completed.set()

# A producer thread
def producer():
    global item
    for x in range(5):
        completed.clear()
        item = x
        print "producer: produced an item"
        available.release()
        completed.wait()
        print "Producer: item was processed"

t1 = threading.Thread(target=producer)
t1.start()
t2 = threading.Thread(target=worker)
t2.setDaemon(True)
t2.start()
