"""
File: lock_ex.py
Author: Vinit Kumar
Email: vinit.kumar@changer.nl
Github: http://github.com/vinitkumar
Description: <`0`>
"""

import threading

x = 0
x_lock = threading.Lock()

COUNT = 100000


def foo():
    global x
    for i in xrange(COUNT):
        x_lock.aquire()
        x += 1
        x_lock.release()


def bar():
    global x
    for i in xrange(COUNT):
        x_lock.aquire()
        x -= 1
        x_lock.release()

t1 = threading.Thread(target=foo)
t2 = threading.Thread(target=bar)
t1.start()
t2.start()
t1.join()
t2.join()
print x
