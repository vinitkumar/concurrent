#! /usr/bin/env python
import threading
import time

items = []

items_cv = threading.Condition()

# A producer thread
def producer():
    print "I am the producer"
    for i in range(30):
        with items_cv:
            items.append(i)
            items_cv.notify()
        time.sleep(1)

# A consumer thread
def consumer():
    print "I am the consumer thread", threading.currentThread().name
    while True:
        with items_cv:
            while not items:
                items_cv.wait()
                x = items.pop(0)

        print threading.currentThread().name, "got", x
        time.sleep(5)

# Launch a bunch of consumers
cons = [threading.Thread(target=consumer) for i in range(10)]

for c in cons:
    c.setDaemon(True)
    c.start()

# Run the producer
producer()