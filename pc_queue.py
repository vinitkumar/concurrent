#! /usr/bin/env python

import threading
import time
import Queue


items = Queue.Queue()

# A producer thread
def producer():
    print "I am the producer"
    for i in range(30):
        items.put(i)
        time.sleep(1)

# A consumer thread
def consumer():
    print "I am a consumer", threading.CurrentThread().name
    while True:
        x = items.get()
        print threading.CurrentThread().name, "got", x
        time.sleep(5)


# Launch a bunch of consumers
cons = [threading.Thread(target=consumer)
        for i in range(10)]

for c in cons:
    c.setDaemon()
    c.start()

# run the producer
producer()