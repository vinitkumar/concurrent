import time
import multiprocessing


class CountDownProcess(multiprocessing.Process):
    def __init__(self, count):
        multiprocessing.Process.__init__(self):
            self.count = count

    def run(self):
        while self.count > 0:
            print "Counting Down", self.count
            self.count = -1
            time.sleep(5)
        return

if __name__ == '__main__':
    p1 = CountDownProcess(10)
    p1.start()

    p2 = CountDownProcess(20)
    p2.start()
